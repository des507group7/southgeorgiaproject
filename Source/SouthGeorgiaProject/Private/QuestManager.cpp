// Fill out your copyright notice in the Description page of Project Settings.


#include "QuestManager.h"
#include <Runtime/Engine/Classes/Kismet/GameplayStatics.h>
#include "UObject/ConstructorHelpers.h"

// Sets default values for this component's properties
UQuestManager::UQuestManager()
{

	static ConstructorHelpers::FClassFinder<UObjectiveUI> QuestUI(TEXT("WidgetBlueprint'/Game/UI/ObjectiveWidgetBP.ObjectiveWidgetBP_C'"));

	if (QuestUI.Class != nullptr)
	{
		_objectiveUIClass = QuestUI.Class;
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to Find Quest UI Class"));
	}
}


void UQuestManager::BeginPlay()
{
	Super::BeginPlay();
	_onQuestTrigger.AddDynamic(this, &UQuestManager::CompleteObjective);

	if (_objectiveUIClass != nullptr)
	{
		_objectiveUI = CreateWidget<UObjectiveUI>(GetWorld()->GetFirstPlayerController(), _objectiveUIClass);

		if (_objectiveUI != nullptr)
		{
			_objectiveUI->AddToViewport();

			if (_objectiveUI != nullptr && _objectivesList.Num() > 0 && _currentObjective < _objectivesList.Num())
				_objectiveUI->UpdateObjective(_objectivesList[_currentObjective]._objectiveDescription);
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("Failed to create Objective UI widget"));
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to add Dialogue UI to viewport"));
	}
}

//Update Quest Values on Load
//Make new public func to loop through all objectives and update the is completed bool
//based on the loaded current objective
void UQuestManager::OnLoad(int current)
{
	_currentObjective = current;

	for (int i = 0; i < _objectivesList.Num(); i++)
	{
		if (i < current)
		{
			_objectivesList[i]._isComplete = true;
		}
		else
		{
			_objectivesList[i]._isComplete = false;
		}
	}


	if (_objectiveUI != nullptr && _objectivesList.Num() > 0 && _currentObjective < _objectivesList.Num())
		_objectiveUI->UpdateObjective(_objectivesList[_currentObjective]._objectiveDescription);
}

void UQuestManager::CompleteObjective(int objective)
{
	UE_LOG(LogTemp, Warning, TEXT("Trigger Quest"));

	if (_objectivesList.Num() > 0 && IsIndexValid(objective))
	{
		if (!_objectivesList[objective]._isComplete)
		{
			//Mark the provided objective as complete
			_objectivesList[objective]._isComplete = true;

			//If this is the current objective then progress the quest
			if (_currentObjective == objective)
				ProgressQuest();
		}
	}


	if (_objectiveUI != nullptr && _objectivesList.Num() > 0 && _currentObjective < _objectivesList.Num())
		_objectiveUI->UpdateObjective(_objectivesList[_currentObjective]._objectiveDescription);
}

void UQuestManager::ProgressQuest()
{
	//If there are objectives in the list and the current objective is within the bounds of the array
	if (_objectivesList.Num() > 0 && IsIndexValid(_currentObjective))
	{
		//Mark the current objective as complete
		_objectivesList[_currentObjective]._isComplete = true;

		//Loop to find the next chronological incomplete quest
		//this accounts for the possibility that the player has completed the subsequent objective early
		for (int i = _currentObjective; i < _objectivesList.Num(); i++)
		{
			if (!_objectivesList[i]._isComplete)
			{
				//Assign the first incomplete objective as the current
				_currentObjective = i;


				if (_objectiveUI != nullptr && _objectivesList.Num() > 0 && _currentObjective < _objectivesList.Num())
					_objectiveUI->UpdateObjective(_objectivesList[_currentObjective]._objectiveDescription);

				//Exit the function
				return;
			}
		}
	}
}


void UQuestManager::AddNewObjective(FString desc, ObjectiveType type)
{
	FObjectiveData newData;
	newData._objectiveDescription = desc;
	newData._objectiveType = type;
	newData._isComplete = false;
	_objectivesList.Add(newData);
}


//This function determines whether the provided index is within the bounds of the objectives array
bool UQuestManager::IsIndexValid(int index)
{
	if (_objectivesList.Num() > 0)
	{
		if (index >= 0 && index < _objectivesList.Num())
			return true;
	}

	return false;
}

FString UQuestManager::GetCurrentObjective()
{
	if (_currentObjective < _objectivesList.Num())
	{
		return _objectivesList[_currentObjective]._objectiveDescription;
	}

	return FString("Quest Complete");
}

TArray<FObjectiveData> UQuestManager::GetAllObjectiveData()
{
	return _objectivesList;
}
