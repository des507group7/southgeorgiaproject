// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerInteraction.h"

// Sets default values for this component's properties
UPlayerInteraction::UPlayerInteraction()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...

	static ConstructorHelpers::FClassFinder<UInteractPrompt> InteractUI(TEXT("WidgetBlueprint'/Game/UI/InteractPromptWidget.InteractPromptWidget_C'"));

	if (InteractUI.Class != nullptr)
	{
		_interactUIClass = InteractUI.Class;
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to Find Interact UI Class"));
	}
}


// Called when the game starts
void UPlayerInteraction::BeginPlay()
{
	Super::BeginPlay();

	// ...
	GetOwner()->InputComponent->BindAction("Interact", IE_Released, this, &UPlayerInteraction::Interact);


	if (_interactUIClass != nullptr)
	{
		_interactUI = CreateWidget<UInteractPrompt>(GetWorld()->GetFirstPlayerController(), _interactUIClass);

		if (_interactUI != nullptr)
		{
			_interactUI->AddToViewport();

			_interactUI->ToggleVisible(false);
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("Failed to create Interact UI widget"));
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to add Interact UI to viewport"));
	}
}


// Called every frame
void UPlayerInteraction::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
	DoRaycast();
}

//This function performs the interactable raycast every frame to determine what the player is looking at
void UPlayerInteraction::DoRaycast()
{
	//Stores of camera transform data
	FVector CamLocation;
	FRotator CamRotation;
	//Get the camera transform
	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(CamLocation, CamRotation);

	//Calculate the start and end points of the raycast
	const FVector StartLocation = CamLocation;
	const FVector Direction = CamRotation.Vector();
	const FVector EndLocation = StartLocation + (Direction * _interactionDistance);

	//Init the collision params
	FCollisionQueryParams TraceParams(FName(TEXT("InteractTrace")), true, NULL);
	TraceParams.bTraceComplex = true;
	TraceParams.bReturnPhysicalMaterial = true;
	TraceParams.AddIgnoredActor(GetOwner());

	//Init the hit result
	FHitResult Hit(ForceInit);

	if(GetWorld()->LineTraceSingleByChannel(Hit, StartLocation, EndLocation, ECC_WorldDynamic, TraceParams))
	{

		if (AInteractable* interactable = Cast<AInteractable>(Hit.Actor))
		{
			_hitInteractable = interactable;

			if (_interactUI != nullptr)
			{
				_interactUI->ToggleVisible(true);
				_interactUI->UpdateUI(_hitInteractable->_prompt);
			}
		}
		else
		{
			//Set stored HitActor to null
			_hitInteractable = nullptr;

			if (_interactUI != nullptr)
			{
				_interactUI->ToggleVisible(false);
				_interactUI->UpdateUI("");
			}
		}
	}
	else
	{
		//Set stored HitActor to null
		_hitInteractable = nullptr;

		if (_interactUI != nullptr)
		{
			_interactUI->ToggleVisible(false);
			_interactUI->UpdateUI("");
		}
	}
}

void UPlayerInteraction::Interact()
{
	if (_hitInteractable != nullptr)
	{
		_hitInteractable->Interact();

		if (_interactUI != nullptr)
		{
			_interactUI->ToggleVisible(false);
			_interactUI->UpdateUI("");
		}
	}
}

