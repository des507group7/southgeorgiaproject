// Fill out your copyright notice in the Description page of Project Settings.


#include "JournalInteractable.h"
#include "Kismet/GameplayStatics.h"
#include "PlayerJournalManager.h"

AJournalInteractable::AJournalInteractable() 
{
	_myCollider = CreateDefaultSubobject<UBoxComponent>(FName("Collision Mesh"));
	_myCollider->SetCollisionProfileName("BlockAllDynamic");
	SetRootComponent(_myCollider);
}

void AJournalInteractable::Interact() 
{
	if (UPlayerJournalManager* journalManager = (UPlayerJournalManager*)UGameplayStatics::GetPlayerPawn(GetWorld(), 0)->GetComponentByClass(UPlayerJournalManager::StaticClass()))
	{
		journalManager->PickupJournal(_journalUI);

		if (_journalPickupSound != nullptr)
			UFMODBlueprintStatics::PlayEventAtLocation(GetWorld(), _journalPickupSound, GetActorTransform(), true);

		//Trigger Spirit Intro


		Destroy();
	}
}