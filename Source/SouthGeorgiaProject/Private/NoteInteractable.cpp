// Fill out your copyright notice in the Description page of Project Settings.


#include "NoteInteractable.h"

ANoteInteractable::ANoteInteractable()
{
	DialogueContainer = CreateDefaultSubobject<UDialogueContainer>(TEXT("Dialogue Container"));

	_myCollider = CreateDefaultSubobject<UBoxComponent>(FName("Collision Mesh"));
	_myCollider->SetCollisionProfileName("BlockAllDynamic");
	SetRootComponent(_myCollider);

}

void ANoteInteractable::Interact()
{
	Super::Interact();

	if (DialogueContainer != nullptr)
		DialogueContainer->SetAsCurrentDialogue();

	if (_notePickupSound != nullptr)
		UFMODBlueprintStatics::PlayEventAtLocation(GetWorld(), _notePickupSound, GetActorTransform(), true);

}
