// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerJournalManager.h"

// Sets default values for this component's properties
UPlayerJournalManager::UPlayerJournalManager()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UPlayerJournalManager::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UPlayerJournalManager::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UPlayerJournalManager::ShowJournal()
{
	if (_journal != nullptr)
	{
		_journal->AddToViewport();
		_isReadingJournal = true;
		_canControlPlayer = false;
	}
}

void UPlayerJournalManager::HideJournal()
{
	if (_journal != nullptr)
	{
		_journal->RemoveFromViewport();
		_isReadingJournal = false;
		_canControlPlayer = true;
	}
}

void UPlayerJournalManager::PickupJournal(UUserWidget* widget)
{
	if (widget != nullptr)
	{
		_journal = widget;
		_hasJournal = true;
		_gameStage = 1;
		ShowJournal();
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Unable to pickup Journal. No Widget Assigned."));
	}
}