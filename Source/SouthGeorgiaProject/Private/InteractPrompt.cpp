// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractPrompt.h"

void UInteractPrompt::UpdateUI(FString prompt)
{
	InteractPrompt->SetText(FText::FromString(*prompt));
}

void UInteractPrompt::ToggleVisible(bool visible)
{
	if (visible)
		SetVisibility(ESlateVisibility::Visible);
	else
		SetVisibility(ESlateVisibility::Hidden);
}