// Fill out your copyright notice in the Description page of Project Settings.


#include "JigsawInteractable.h"
#include "JigsawManager.h"
AJigsawInteractable::AJigsawInteractable()
{
	JigsawContainer = CreateDefaultSubobject<UJigsawContainer>(TEXT("Jigsaw Container"));

	_myCollider = CreateDefaultSubobject<UBoxComponent>(FName("Collision Mesh"));
	_myCollider->SetCollisionProfileName("BlockAllDynamic");
	SetRootComponent(_myCollider);
}

void AJigsawInteractable::Interact()
{
	if (JigsawContainer->_isCompleted)
		return;

	Super::Interact();


	if (UJigsawManager* manager = (UJigsawManager*)GetWorld()->GetFirstPlayerController()->GetPawn()->GetComponentByClass(UJigsawManager::StaticClass()))
	{
		if (JigsawContainer != nullptr && !manager->GetPuzzleStatus())
			JigsawContainer->StartPuzzle();

		if (_beginPuzzleSound != nullptr)
			UFMODBlueprintStatics::PlayEventAtLocation(GetWorld(), _beginPuzzleSound, GetActorTransform(), true);
	}
}