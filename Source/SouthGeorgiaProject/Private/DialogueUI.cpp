// Fill out your copyright notice in the Description page of Project Settings.


#include "DialogueUI.h"

void UDialogueUI::UpdateUI(FString speaker, FString contents)
{
	SpeakerName->SetText(FText::FromString(*speaker));
	Subtitle->SetText(FText::FromString(*contents));
}

void UDialogueUI::ToggleVisible(bool visible)
{
	if(visible)
		SetVisibility(ESlateVisibility::Visible);
	else
		SetVisibility(ESlateVisibility::Hidden);
}