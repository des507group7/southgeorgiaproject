// Fill out your copyright notice in the Description page of Project Settings.


#include "Interactable.h"

// Sets default values
AInteractable::AInteractable()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	/*_myCollider = CreateDefaultSubobject<UBoxComponent>(FName("Collision Mesh"));
	_myCollider->SetCollisionProfileName("BlockAllDynamic");
	SetRootComponent(_myCollider);*/
}

// Called when the game starts or when spawned
void AInteractable::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AInteractable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AInteractable::Interact()
{
	UE_LOG(LogTemp, Warning, TEXT("Interacting with %s"), *GetName());
	BlueprintInteract();
}

