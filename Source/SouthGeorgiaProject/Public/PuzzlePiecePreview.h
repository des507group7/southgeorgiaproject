// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/SizeBox.h"
#include "Components/Image.h"
#include "JigsawPiece.h"
#include "PuzzlePiecePreview.generated.h"

/**
 * 
 */
UCLASS()
class SOUTHGEORGIAPROJECT_API UPuzzlePiecePreview : public UUserWidget
{
	GENERATED_BODY()
	
public:

	virtual void NativeConstruct() override;

	UPROPERTY(meta = (BindWidget))
		USizeBox* WidgetSize;

	UPROPERTY(meta = (BindWidget))
		UImage* PreviewImage;

	UPROPERTY()
		UUserWidget* _widgetReference;


};
