// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "MySaveGame.generated.h"

/**
 * 
 */
UCLASS()
class SOUTHGEORGIAPROJECT_API UMySaveGame : public USaveGame
{
	GENERATED_BODY()
	
public:
	UMySaveGame();

	//This is where the savable properties are declared
	UPROPERTY(EditAnywhere)
		FVector _playerPosition;
	UPROPERTY(EditAnywhere)
		FRotator _playerRotation;
	UPROPERTY(EditAnywhere)
		int _currentObjectiveIndex;
	UPROPERTY(EditAnywhere)
		int _currentTimePeriodIndex;


};
