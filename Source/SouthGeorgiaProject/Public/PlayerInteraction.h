// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Interactable.h"
#include "InteractPrompt.h"
#include "PlayerInteraction.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SOUTHGEORGIAPROJECT_API UPlayerInteraction : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPlayerInteraction();
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
	
	void DoRaycast();
	void Interact();

	AInteractable* _hitInteractable;

	UPROPERTY(EditAnywhere)
		float _interactionDistance = 500.0f;

	UInteractPrompt* _interactUI;
	TSubclassOf<class UInteractPrompt> _interactUIClass;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;		
};
