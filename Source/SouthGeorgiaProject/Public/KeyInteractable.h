// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "DoorInteractable.h"
#include "KeyInteractable.generated.h"

/**
 * 
 */
UCLASS()
class SOUTHGEORGIAPROJECT_API AKeyInteractable : public AInteractable
{
	GENERATED_BODY()
public:
	AKeyInteractable();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		ADoorInteractable* _linkedDoor;
	
	UBoxComponent* _myCollider = nullptr;

	virtual void Interact() override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UFMODEvent* _keyPickupSound;
};
