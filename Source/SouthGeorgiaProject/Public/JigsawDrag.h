// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/DragDropOperation.h"
#include "Blueprint/UserWidget.h"
#include "JigsawDrag.generated.h"

/**
 * 
 */
UCLASS()
class SOUTHGEORGIAPROJECT_API UJigsawDrag : public UDragDropOperation
{
	GENERATED_BODY()
	
public:
	UPROPERTY()
		FVector2D _dragOffset;
	UPROPERTY()
		UUserWidget* _widgetReference;
	UPROPERTY()
		FVector2D _widgetSize;

};
