// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "InteractPrompt.generated.h"

/**
 * 
 */
UCLASS()
class SOUTHGEORGIAPROJECT_API UInteractPrompt : public UUserWidget
{
	GENERATED_BODY()
	
public:

	UPROPERTY(meta = (BindWidget))
		UTextBlock* InteractPrompt;

	UFUNCTION()
		void UpdateUI(FString prompt);

	UFUNCTION()
		void ToggleVisible(bool visible);

};
